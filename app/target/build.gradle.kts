import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.3"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.21"
	kotlin("plugin.spring") version "1.5.21"
}

group = "com.book"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	//implementation ("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.hibernate:hibernate-core:5.4.0.Final")
	implementation("org.flywaydb:flyway-core")
	implementation("javax.xml.bind:jaxb-api:2.3.1")
	implementation("org.springframework.boot:spring-boot-starter-quartz")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-jdbc")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("mysql:mysql-connector-java")
	implementation ("io.ktor:ktor-client-okhttp:1.3.1")
	implementation ("io.ktor:ktor-client-gson:1.3.1")
	implementation ("org.springframework:spring-context-support")
	implementation ("io.ktor:ktor-client-core:1.6.2")
	implementation ("io.ktor:ktor-client-cio:1.6.2")
	implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.14.0")
	implementation (group= "com.bedatadriven", name = "jackson-datatype-jts", version = "2.4")
	implementation (group= "io.ktor", name = "ktor-client-core", version = "1.3.1")
	implementation (group= "redis.clients", name = "jedis", version = "3.3.0")
	implementation (group= "org.quartz-scheduler", name = "quartz", version = "2.3.0")
	implementation (group= "com.google.code.gson", name = "gson", version = "2.8.6")
	implementation( group = "com.github.javafaker", name= "javafaker", version= "0.15")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}


tasks {
	bootJar {
		archiveFileName.set("app.jar")
	}
}


tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
