package com.book.demo.unit.module.contract

import com.book.demo.common.util.cache.CacheUtil
import com.book.demo.common.util.job.JobUtil
import com.book.demo.common.util.log.LogUtil
import com.book.demo.module.contract.book.BookContract
import com.book.demo.module.model.book.Book
import com.book.demo.module.repository.book.BookRepository
import com.book.demo.unit.BaseUnitTestCase
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.*
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.`when`
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import java.lang.reflect.Type


class BookContractTest:BaseUnitTestCase() {




    @Mock
    private lateinit var repository: BookRepository

    @Mock
    private lateinit var cacheUtil: CacheUtil

    @Mock
    private lateinit var jobUtil: JobUtil





    @InjectMocks
    private lateinit var bookContract: BookContract;






    @Test
    fun testCreate()
    {


        val book = Book()
        book.name = "zoovierre"


        `when`(repository.save(any(Book::class.java)))
            .thenAnswer(Answer { i: InvocationOnMock -> book })

        val obj = bookContract.create(book.name!!)

        assertNotNull(obj)
        assertTrue(obj is Book)
    }


    @Test
    fun testGet()
    {


//        val book = Book()
//        book.id = 1L
//        book.name = "zoovierre"
//
//
//       `when`(cacheUtil.get(anyString(),any(Type::class.java))).thenReturn(null)
//        `when`(repository.getById(anyLong())).thenReturn(book)
//
//
//
//
//
//
//        val obj = bookContract.get(book.id!!)
//
//        assertNotNull(obj)
//        assertTrue(obj is Book)
    }
}