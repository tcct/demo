package com.book.demo.unit

import com.book.demo.common.util.log.LogUtil
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource



@TestPropertySource("classpath:application_test.properties")
abstract class BaseUnitTestCase {


    @Mock
    protected  lateinit var logUtil: LogUtil



    @BeforeEach
    fun init() {
        MockitoAnnotations.initMocks(this)

        logUtil = LogUtil()
    }

     fun <T> any(type: Class<T>): T = Mockito.any<T>(type)

}