package com.book.demo.intergrated.module.book

import com.book.demo.common.util.log.LogUtil
import com.book.demo.intergrated.BaseIntergratedTestCase
import com.book.demo.module.contract.book.BookContract
import com.book.demo.module.model.book.Book
import com.book.demo.module.repository.book.BookRepository
import org.hamcrest.CoreMatchers.containsString
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import  org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers

class BookRestTest:BaseIntergratedTestCase() {


    @Autowired
    lateinit var repository:BookRepository

    private lateinit var book: Book

    @BeforeEach
    fun startup() {

        book = repository.findAll()[0]

    }


    @Test
    @Throws(Exception::class)
    fun testBookCreate() {

        val name = generatorUtil.generateString(8)!!

        this.mockMvc.perform(post("/api/book/v1/").requestAttr("name",name)).andDo(MockMvcResultHandlers.print())
            .andExpect(status().isCreated)


    }

    @Test
    @Throws(Exception::class)
    fun testBookCreateJobScheduler() {

        val name = generatorUtil.generateString(8)!!

        this.mockMvc.perform(post("/api/book/v1/job/scheduler").requestAttr("name",name)).andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)


    }

    @Test
    @Throws(Exception::class)
    fun testBookCreateJobCron() {

        val name = generatorUtil.generateString(8)!!

        this.mockMvc.perform(post("/api/book/v1/job/cron").requestAttr("name",name)).andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)


    }

    @Test
    @Throws(Exception::class)
    fun testBookCreateUnique() {



        this.mockMvc.perform(post("/api/book/v1/").requestAttr("name",book.name!!)).andDo(MockMvcResultHandlers.print())
            .andExpect(status().isBadRequest)


    }


    @Test
    @Throws(Exception::class)
    fun testBookUpdate() {



        this.mockMvc.perform(put("/api/book/v1/update").requestAttr("uuid",book.uuid.toString()).requestAttr("name",generatorUtil.generateString(6)!!)).andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)

    }

    @Test
    @Throws(Exception::class)
    fun testBookGet() {

        logUtil.logging(book.uuid.toString())


        this.mockMvc.perform(get("/api/book/v1/{uuid}",book.uuid.toString())).andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)

    }

    @Test
    @Throws(Exception::class)
    fun testBookGetAll() {



        this.mockMvc.perform(get("/api/book/v1/all/{page}",1)).andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)

    }


    @Test
    @Throws(Exception::class)
    fun testBookDelete() {



            this.mockMvc.perform(put("/api/book/v1/delete").requestAttr("uuid",book.uuid.toString())).andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk)

    }


}