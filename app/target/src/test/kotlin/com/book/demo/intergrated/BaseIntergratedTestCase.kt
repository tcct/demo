package com.book.demo.intergrated

import com.book.demo.DemoApplication
import com.book.demo.common.util.generator.GeneratorUtil
import com.book.demo.common.util.log.LogUtil
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import javax.transaction.Transactional


@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@TestPropertySource("classpath:application_test.properties")
abstract class BaseIntergratedTestCase {


    @Autowired
    protected  lateinit var generatorUtil: GeneratorUtil

    @Autowired
    protected  lateinit var logUtil: LogUtil

    @Autowired
    protected lateinit var mockMvc: MockMvc


    @BeforeEach
    fun init() {
        MockitoAnnotations.initMocks(this)

        logUtil = LogUtil()
    }

}