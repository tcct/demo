CREATE TABLE IF NOT EXISTS `books` (

    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `uuid` BINARY(16) NOT NULL UNIQUE,
    `name` varchar(50) NOT NULL UNIQUE,
    `deleted` TINYINT NOT NULL DEFAULT 0,
    `created_at` timestamp,
    `updated_at` timestamp

)ENGINE=InnoDB DEFAULT CHARSET=UTF8;