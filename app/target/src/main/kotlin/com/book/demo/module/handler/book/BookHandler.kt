package com.book.demo.module.handler.book

import com.book.demo.common.util.generator.GeneratorUtil
import com.book.demo.common.util.job.JobUtil
import com.book.demo.common.util.log.LogUtil
import com.book.demo.module.job.book.BookCreateJob
import org.quartz.CronScheduleBuilder
import org.quartz.Job
import org.quartz.JobDataMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*


@Component
class BookHandler {


    @Autowired
    private lateinit var logUtil: LogUtil

    @Autowired
    private lateinit var jobUtil: JobUtil

    @Autowired
    private lateinit var generatorUtil: GeneratorUtil

    private val jobDataMap = JobDataMap()

    private val KEY_PREFIX = "Book:"
    private val KEY_GROUP_PREFIX = "Books:"

    val NAME_KEY = "name"

    fun createByScheduledJob(name:String):Boolean
    {

        try{

            val key = StringBuilder().append(KEY_PREFIX).append(generatorUtil.generateUUID()!!).toString()
            val keyGroup = KEY_GROUP_PREFIX


            jobDataMap[NAME_KEY] = name


            val date = Date(Date().time + 2000)
           return jobUtil.createScheduledJob(key,keyGroup, BookCreateJob::class.java as Class<Job>,jobDataMap,date,false)



        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

        }

        return false


    }


    fun createByCronJob(name:String):Boolean
    {

        try{

            val key = StringBuilder().append(KEY_PREFIX).append(generatorUtil.generateUUID()!!).toString()
            val keyGroup = KEY_GROUP_PREFIX


            jobDataMap[NAME_KEY] = name


            return jobUtil.createCronJob(key,keyGroup, BookCreateJob::class.java as Class<Job>,jobDataMap,"0/2 0 0 ? * * *")




        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

        }

        return false


    }

}