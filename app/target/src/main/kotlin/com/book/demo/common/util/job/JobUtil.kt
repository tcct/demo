package com.book.demo.common.util.job

import com.book.demo.common.util.log.LogUtil
import com.book.demo.module.job.book.BookCreateJob
import org.quartz.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*
import kotlin.reflect.KClass


@Component
class JobUtil {


    @Autowired
    private lateinit var logUtil: LogUtil


    @Autowired
    private val scheduler: Scheduler? = null


    fun createScheduledJob(
        key:String,
        keyGroup:String,
        jobClass: Class<Job>,
        data: JobDataMap,
        time:Date,
        immediately:Boolean):Boolean
    {
        try{


            val jobDetail = setJobDetail(key,keyGroup,jobClass,data)
            val trigger = setTriggerTime(key,keyGroup,jobDetail!!,time,immediately)

            return initJob(jobDetail,trigger!!)
        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false
    }


    fun createCronJob(key:String,keyGroup:String,jobClass:Class<Job>,data:JobDataMap,cron: String):Boolean
    {
        try{


            val jobDetail = setJobDetail(key,keyGroup,jobClass,data)
            val trigger = setTriggerCron(key,keyGroup,jobDetail!!, CronScheduleBuilder.cronSchedule(cron))

            return initJob(jobDetail,trigger!!)
        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false
    }


    fun setJobDetail(key:String,keyGroup:String,jobClass:Class<Job>,data:JobDataMap): JobDetail?
    {
        try{



            return JobBuilder.newJob().ofType(jobClass)
                .storeDurably()
                .withIdentity(key,keyGroup)
                .setJobData(data)
                .requestRecovery(true)
                .build();
        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }


    fun setTriggerTime(key:String,keyGroup:String,job:JobDetail,time:Date,immediately: Boolean):Trigger?
    {
        try{

            if(immediately)
            {
                return TriggerBuilder.newTrigger().forJob(job)
                    .withIdentity(key,keyGroup)
                    .startAt(time)
                    .build();
            }
            else
            {
                return TriggerBuilder.newTrigger().forJob(job)
                    .withIdentity(key,keyGroup)
                    .startNow()
                    .build();
            }


        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }

    fun setTriggerCron(key:String,keyGroup:String,job:JobDetail,scheduler:CronScheduleBuilder):Trigger?
    {
        try{


            return TriggerBuilder.newTrigger().forJob(job)
                .withIdentity(key,keyGroup)
                .withSchedule(scheduler)
                .build();
        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }

    fun initJob(job:JobDetail,trigger: Trigger):Boolean
    {
        try{

            scheduler?.scheduleJob(job, trigger);
            scheduler?.start();

            return true
        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false
    }

}