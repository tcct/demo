package com.book.demo.common.util.json

import com.book.demo.common.util.log.LogUtil
import com.google.gson.Gson
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.reflect.Type

@Service
class JsonUtil {



    @Autowired
    private lateinit var gson: Gson

    @Autowired
    private lateinit var logUtil: LogUtil

    fun convertToString(data:Any):String?
    {
        try{
            return gson.toJson(data)
        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }

    fun convertToObject(data:String,type: Type):Any?
    {

        try{
            return gson.fromJson(data, type)

        }
        catch(ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null


    }


}