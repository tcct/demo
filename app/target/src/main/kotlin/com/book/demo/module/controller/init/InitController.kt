package com.book.demo.module.controller.init

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController



@RestController
class InitController {



    @GetMapping("/")
    fun index(): String? {
        return "Greetings from the other side!"
    }
}