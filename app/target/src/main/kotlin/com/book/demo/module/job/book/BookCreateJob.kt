package com.book.demo.module.job.book

import com.book.demo.module.handler.notification.NotificationHandler
import com.book.demo.common.util.generator.GeneratorUtil
import com.book.demo.common.util.log.LogUtil
import com.book.demo.module.contract.book.BookContract
import com.book.demo.module.handler.book.BookHandler
import org.quartz.Job
import org.quartz.JobDataMap
import org.quartz.JobExecutionContext
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class BookCreateJob(): InitializingBean, Job {





    @Autowired
    private  lateinit var contract: BookContract

    @Autowired
    private  lateinit var handler:BookHandler

    @Autowired
    private  lateinit var logUtil: LogUtil

    @Autowired
    private  lateinit var generatorUtil: GeneratorUtil

    private var jobDataMap: JobDataMap?= null





    override fun execute(context: JobExecutionContext?) {

        try{

            jobDataMap = context?.mergedJobDataMap



            run()


        }
        catch (ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

    }

    private fun run():Boolean
    {
        try{



            val name = jobDataMap!![handler.NAME_KEY] as String

            contract.create(name)


            return true
        }
        catch (ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false
    }



    override fun afterPropertiesSet() {

    }


}