package com.book.demo.module.converter

import com.book.demo.common.util.log.LogUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Component
class BooleanIntConverter:AttributeConverter<Boolean,Int> {


    @Autowired
    lateinit var logUtil: LogUtil

    override fun convertToDatabaseColumn(attribute: Boolean?): Int {

        try{

            when(attribute!!)
            {
                true ->{
                    return 1
                }

                else ->{
                    return 0
                }
            }


        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

        }

       return 0

    }

    override fun convertToEntityAttribute(dbData: Int?): Boolean {

        try{

            when(dbData!!)
            {
                1 ->{
                    return true
                }

                else ->{
                    return false
                }
            }

        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

        }

        return false


    }
}