package com.book.demo.module.repository.book

import com.book.demo.module.model.book.Book
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BookRepository : JpaRepository<Book, Long> {



    fun findFirstByUuidAndDeleted(uuid: UUID,deleted: Boolean):Book?
    fun findByDeleted(deleted:Boolean,pageable: Pageable):Page<Book>?

}