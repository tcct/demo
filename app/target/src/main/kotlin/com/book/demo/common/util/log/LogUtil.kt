package com.book.demo.common.util.log

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component


@Component
class LogUtil {


    private val LOG_NAME = "APP_LOG - "
    private val LOG_EXCEPTION_NAME = "APP_LOG_EXCEPTION - "
    private val METHOD = "METHOD:"

    companion object {
        private var logger  = LoggerFactory.getLogger(LogUtil::class.java)
    }


    fun logging(value:String)
    {
        try {

            val error = StringBuilder().append(LOG_NAME).append(value).toString()

            logger.error(error)

        }
        catch (ex:Exception)
        {


        }

    }




    fun exception(ex:Exception,methodName:String = "")
    {
        try {


            val error = StringBuilder().append(LOG_EXCEPTION_NAME).append(METHOD).append(methodName).append(" - ").append(ex.message).toString()

            logger.error(error)


        }
        catch (ex:Exception)
        {


        }
    }
}