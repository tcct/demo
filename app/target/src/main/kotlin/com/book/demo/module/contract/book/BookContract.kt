package com.book.demo.module.contract.book

import com.book.demo.common.util.cache.CacheUtil
import com.book.demo.common.util.generator.GeneratorUtil
import com.book.demo.common.util.job.JobUtil
import com.book.demo.module.model.book.Book
import com.book.demo.module.repository.book.BookRepository
import com.book.demo.common.util.log.LogUtil
import com.book.demo.module.handler.book.BookHandler
import com.book.demo.module.value.pagination.PaginationValue
import com.book.demo.module.job.book.BookCreateJob
import com.book.demo.module.value.cache.CacheValue
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.quartz.Job
import org.quartz.JobDataMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import java.lang.reflect.Type
import java.util.*


@Component
class BookContract {

    @Autowired
     lateinit var logUtil:LogUtil

    @Autowired
    lateinit var repository: BookRepository

    @Autowired
    lateinit var cacheUtil:CacheUtil

    @Autowired
    lateinit var handler:BookHandler

    private var cacheName:String ?= null





    private val objType: Type = object : TypeToken<Book>() {}.type
    private val objListType: Type = object : TypeToken<Page<Book>>() {}.type





    fun seedRequired():Boolean
    {

        try{

            return   repository.count() < 1


        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

        }

        return false

    }



    fun createByScheduledJob(name:String):Boolean
    {

        try{


            val created = handler.createByScheduledJob(name)


            return  created


        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

        }

        return false


    }

    fun createByCronJob(name:String):Boolean
    {

        try{


            val created = handler.createByCronJob(name)


            return  created


        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

        }

        return false


    }





    fun create(name:String): Any
    {
        try{


            val obj = Book()
            obj.name = name

            return  repository.save(obj)


        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)


            return ex.javaClass.name
        }


    }

    fun update(uuid:UUID,name:String): Any
    {
        try{

            cacheName = StringBuilder().append(CacheValue.book).append(uuid).toString()

            //get object
            val obj = get(uuid)

            when(obj)
            {

                is Book ->{

                    //alter object & save
                    obj.name = name
                    repository.save(obj)

                    //coroutine
                    GlobalScope.async{

                        //remove from cache
                        cacheUtil.remove(cacheName!!)
                    }

                    return true

                }
                else ->{

                    return false

                }
            }


        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)


            return ex.javaClass.name
        }


    }


    fun get(uuid:UUID): Any
    {
        try{

            //get from cache
                cacheName = StringBuilder().append(CacheValue.book).append(uuid).toString()
            var obj= cacheUtil.get(cacheName!!,objType) as Book?


            if (obj == null)
            {
                //get from repository
                obj = repository.findFirstByUuidAndDeleted(uuid,false)

                //save to cache
                GlobalScope.async {

                    cacheUtil.save(cacheName!!,obj!!,cacheUtil.MIN_SAVE)
                }
            }

            return  obj as Book

        }
        catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

            return ex.javaClass.name
        }


    }


    fun getAll(page:Int): Any {
        try{

            //create pager
            val pageable = PageRequest.of(page, PaginationValue.MIN)

            //get from cache
            cacheName = StringBuilder().append(CacheValue.books).append(page).toString()
            var obj = cacheUtil.get(cacheName!!,objListType)

            if (obj == null)
            {
                //get from repository
                obj = repository.findByDeleted(false,pageable)

                //save to cache
                GlobalScope.async {

                    cacheUtil.save(cacheName!!,obj!!,cacheUtil.MIN_SAVE)
                }

            }

            return  obj as Page<Book>



        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

            return ex.javaClass.name
        }

    }


    fun delete(uuid: UUID): Any {
        try{

            cacheName = StringBuilder().append(CacheValue.book).append(uuid).toString()

            val obj = get(uuid)

            when(obj)
            {

                is Book ->{

                    //alter object & save
                    obj.deleted = true
                    repository.save(obj)

                    //coroutine
                    GlobalScope.async{

                        //remove from cache
                        cacheUtil.remove(cacheName!!)
                    }

                    return true

                }
                else ->{

                    return false

                }
            }



        }catch (ex:Exception)
        {

            logUtil.exception(ex,object{}.javaClass.enclosingMethod.name)

            return ex.javaClass.name
        }

    }

}