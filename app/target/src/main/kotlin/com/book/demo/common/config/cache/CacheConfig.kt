package com.book.demo.common.config.cache

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import java.time.Duration

@Component
@PropertySource("classpath:application.properties")
class CacheConfig {


    @Value("\${cache.host}")
    private lateinit var host:String

    @Value("\${cache.password}")
    private  lateinit var password:String

    @Value("\${cache.port}")
    private lateinit var port:String


    @Bean
    fun jedis(): Jedis
    {


        val jedis =  initJedisPool().resource
        return jedis
    }




    private fun initJedisPool(): JedisPool
    {


        return JedisPool(initJedisPoolConfig(),host, port.toInt(),10000,password);
    }



    private fun initJedisPoolConfig(): JedisPoolConfig
    {
        val poolConfig = JedisPoolConfig()
        poolConfig.maxTotal = 128;
        poolConfig.maxIdle = 128;
        poolConfig.minIdle = 16;
        poolConfig.testOnBorrow = true;
        poolConfig.testOnReturn = true;
        poolConfig.testWhileIdle = true;
        poolConfig.minEvictableIdleTimeMillis = Duration.ofSeconds(60).toMillis();
        poolConfig.timeBetweenEvictionRunsMillis = Duration.ofSeconds(30).toMillis();
        poolConfig.numTestsPerEvictionRun = 3;
        poolConfig.blockWhenExhausted = true;


        return poolConfig
    }

}