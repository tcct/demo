package com.book.demo.common.util.generator

import com.book.demo.common.util.log.LogUtil
import net.bytebuddy.utility.RandomString
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*


@Component
class GeneratorUtil {


    @Autowired
    private  lateinit var logUtil: LogUtil


    fun generateUUID(): UUID?{

        try{

            return UUID.randomUUID()
        }
        catch (ex:Exception)
        {
            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }

    fun convertToUUID(value:String): UUID?{

        try{

            return UUID.fromString(value)
        }
        catch (ex:Exception)
        {
            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }

    fun generateString(length:Int): String?{

        try{

            return RandomString.make(length)
        }
        catch (ex:Exception)
        {
            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }

}