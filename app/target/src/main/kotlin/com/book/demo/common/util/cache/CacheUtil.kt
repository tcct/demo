package com.book.demo.common.util.cache

import com.book.demo.common.util.json.JsonUtil
import com.book.demo.common.util.log.LogUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import redis.clients.jedis.Jedis
import java.lang.reflect.Type

@Service
class CacheUtil {


    @Autowired
    private lateinit var logUtil: LogUtil


    @field:Autowired
    private lateinit var jedis: Jedis

    @Autowired
    private lateinit var jsonUtil: JsonUtil

    val MIN_SAVE = 2 * 1000

    val MED_SAVE = 4 * 1000
    val MAX_SAVE = 8 * 1000



    fun save(key:String,data:Any,duration:Int):Boolean
    {

        try{

            jedis.set(key,jsonUtil.convertToString(data))
            jedis.expire(key,duration)

            return true
        }
        catch (ex:Exception)
        {
            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false

    }


    fun saveForever(key:String,data:Any):Boolean
    {

        try{

            jedis.set(key,jsonUtil.convertToString(data))


            return true
        }
        catch (ex:Exception)
        {
            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false

    }

    fun get(key:String,type: Type):Any?
    {

        try{


            val json = jedis.get(key)


            return jsonUtil.convertToObject(json,type)


        }
        catch (ex:Exception)
        {
            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return null
    }


    fun remove(key:String):Boolean
    {

        try{
            jedis.del(key)

            return true
        }
        catch (ex:Exception)
        {
            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false
    }



}