package com.book.demo.module.model.book

import com.book.demo.module.converter.BooleanIntConverter
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.UpdateTimestamp

import java.util.*
import javax.persistence.*


@Entity
@Table(name = "Books")
class Book {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null


    @Column(columnDefinition = "BINARY(16)",nullable = false, updatable = false,unique = true)
    var uuid: UUID = UUID.randomUUID()




    @Column(nullable = false, updatable = true)
    var name: String? = null

    @Column(nullable = false, updatable = true)
    @JsonIgnore
    @Convert(converter = BooleanIntConverter::class)
    var deleted: Boolean = false

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    var created_at:Date ?= null

    @Column(nullable = false, updatable = true)
    @UpdateTimestamp
    var updated_at:Date ?= null
}