package com.book.demo.common.util.response

import com.book.demo.common.util.log.LogUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component


@Component
class ResponseUtil {

    @Autowired
    lateinit var logUtil: LogUtil



    companion object{
        private val map = HashMap<String,Any?>()
        private val CODE = "code"
        private val ERROR = "error"
        private val DATA = "data"

    }

    init {

    }

    fun error():ResponseEntity<Any>
    {

        return failed(HttpStatus.INTERNAL_SERVER_ERROR,null)


    }



    fun success(code:HttpStatus, body:Any?): ResponseEntity<Any> {
        try {

            map.clear()
            map[CODE] = code.value()

            if(body != null)
            {
                map[DATA] = body
            }


            return  ResponseEntity
                .status(code)
                .body(map)

        }
        catch (ex:Exception)
        {
            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )

        }

        return error()

    }


    fun failed(code:HttpStatus, error:Any?): ResponseEntity<Any> {
        try {


            map.clear()
            map[CODE] = code.value()

            if(error != null)
            {
                map[ERROR] = error
            }



            return  ResponseEntity
                .status(code)
                .body(map)


        }
        catch (ex:Exception)
        {
            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )

        }

        return error()

    }
}