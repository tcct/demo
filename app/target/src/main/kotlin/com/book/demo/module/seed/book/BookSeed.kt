package com.book.demo.module.seed.book

import com.book.demo.common.util.generator.GeneratorUtil
import com.book.demo.common.util.log.LogUtil
import com.book.demo.module.contract.book.BookContract
import net.bytebuddy.utility.RandomString
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service


@Service
@PropertySource("classpath:application.properties")
class BookSeed {

    @Autowired
    lateinit var logUtil: LogUtil

    @Autowired
    lateinit var contract: BookContract

    @Autowired
    lateinit var generatorUtil:GeneratorUtil

    @Value("\${app.debug}")
     var debug: Boolean = false




    @EventListener
    fun seed(event:ContextRefreshedEvent)
    {

        try{


            when(debug)
            {
                true ->{

                    if(contract.seedRequired())
                    {
                        (0..9).forEach { _ ->

                            contract.create(generatorUtil.generateString(5)!!)

                        }
                    }

                }

                else ->
                {


                }

            }

        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )
        }


    }

}