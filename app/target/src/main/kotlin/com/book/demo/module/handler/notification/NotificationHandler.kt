package com.book.demo.module.handler.notification

import com.book.demo.common.util.log.LogUtil
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.xml.bind.JAXBElement


@Component
class NotificationHandler {



    @Autowired
    private  lateinit var logUtil: LogUtil



     fun send(primary:String,secondary:String,tertiary:String):Boolean {

        try{

            GlobalScope.launch {

                val client = HttpClient(CIO)
                val response: HttpResponse = client.get("https://ktor.io/")

                println(response.status)
                client.close()

            }



            return true
        }
        catch (ex:Exception)
        {

            logUtil.exception(ex, object{}.javaClass.enclosingMethod.name)

        }

        return false
    }
}