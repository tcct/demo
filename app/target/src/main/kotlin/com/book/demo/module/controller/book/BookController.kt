package com.book.demo.module.controller.book

import com.book.demo.common.util.generator.GeneratorUtil
import com.book.demo.common.util.log.LogUtil
import com.book.demo.common.util.response.ResponseUtil
import com.book.demo.module.contract.book.BookContract
import com.book.demo.module.model.book.Book
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/book/v1")
class BookController {


    @Autowired
    lateinit var logUtil: LogUtil

    @Autowired
    lateinit var bookContract: BookContract

    @Autowired
    lateinit var responseUtil: ResponseUtil


    @Autowired
    lateinit var generatorUtil: GeneratorUtil


    @PostMapping("/")
    fun create(@RequestAttribute("name") name:String): ResponseEntity<Any>
    {
        try{

            val obj = bookContract.create(name)


            when(obj)
            {

                is Book ->{

                    return  responseUtil.success(HttpStatus.CREATED,obj)

                }

                else -> {

                    return  responseUtil.failed(HttpStatus.BAD_REQUEST,obj)


                }
            }

        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )

         
        }

        return responseUtil.error()


    }


    @PostMapping("/job/scheduler")
    fun createByScheduler(@RequestAttribute("name") name:String): ResponseEntity<Any>
    {
        try{

            val obj = bookContract.createByScheduledJob(name)


            when(obj)
            {

                true ->{

                    return  responseUtil.success(HttpStatus.OK,null)

                }

                else -> {

                    return  responseUtil.failed(HttpStatus.BAD_REQUEST,null)


                }
            }

        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )


        }

        return responseUtil.error()


    }

    @PostMapping("/job/cron")
    fun createByCron(@RequestAttribute("name") name:String): ResponseEntity<Any>
    {
        try{

            val obj = bookContract.createByCronJob(name)


            when(obj)
            {

                true ->{

                    return  responseUtil.success(HttpStatus.OK,null)

                }

                else -> {

                    return  responseUtil.failed(HttpStatus.BAD_REQUEST,null)


                }
            }

        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )


        }

        return responseUtil.error()


    }


    @PutMapping("update")
    fun update(@RequestAttribute("uuid") uuid:String,@RequestAttribute("name") name:String): ResponseEntity<Any>
    {
        try{

            val obj = bookContract.update(generatorUtil.convertToUUID(uuid)!!,name)

            when(obj)
            {

                true ->{

                    return  responseUtil.success(HttpStatus.OK,obj)

                }

                else -> {

                    return  responseUtil.failed(HttpStatus.BAD_REQUEST,obj)

                }
            }




        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )

            return responseUtil.error()
        }



    }

    @GetMapping("/{uuid}")
    fun get(@PathVariable("uuid") uuid:String): ResponseEntity<Any>
    {
        try{

            logUtil.logging(generatorUtil.convertToUUID(uuid)!!.toString())

            val obj = bookContract.get(generatorUtil.convertToUUID(uuid)!!)

            when(obj)
            {

                is Book ->{

                    return  responseUtil.success(HttpStatus.OK,obj)

                }

                else -> {

                    return  responseUtil.failed(HttpStatus.BAD_REQUEST,obj)


                }
            }




        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )

            return responseUtil.error()
        }



    }

    @GetMapping("all/{page}")
    fun getAll(@PathVariable("page") page:Int): ResponseEntity<Any>
    {
        try{

            val obj = bookContract.getAll(page)

            when(obj)
            {

                is Page<*> ->{

                    return  responseUtil.success(HttpStatus.OK,obj)

                }

                else -> {

                    return  responseUtil.failed(HttpStatus.BAD_REQUEST,obj)


                }
            }




        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )

            return responseUtil.error()
        }



    }


    @PutMapping("delete")
    fun delete(@RequestAttribute("uuid") uuid:String): ResponseEntity<Any>
    {
        try{

            val obj = bookContract.delete(generatorUtil.convertToUUID(uuid)!!)

            when(obj)
            {

                true ->{

                    return  responseUtil.success(HttpStatus.OK,null)

                }

                else -> {

                    return  responseUtil.failed(HttpStatus.BAD_REQUEST,obj)

                }
            }




        }catch (ex:Exception)
        {

            logUtil.exception(ex,Object()
                .getClass()
                .enclosingMethod
                .name
            )

            return responseUtil.error()
        }



    }




}